import 'package:calories_calculator/constants.dart';
import 'package:calories_calculator/widget/profile_widget.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class EditProfile extends StatefulWidget {
  String userId;
  EditProfile({Key? key, required this.userId}) : super(key: key);

  @override
  _EditProfileState createState() => _EditProfileState(this.userId);
}

class _EditProfileState extends State<EditProfile> {
  String height = '';
  String weight = '';
  String age = '';
  String gender = 'Male';
  late String userId;
  String? _imageURL = '';
  String? _name = '';
  String? _email = '';
  late final Stream<QuerySnapshot> _profileStream;

  CollectionReference users = FirebaseFirestore.instance.collection('users');
  _EditProfileState(this.userId);
  TextEditingController _heightController = new TextEditingController();
  TextEditingController _weightController = new TextEditingController();
  TextEditingController _ageController = new TextEditingController();

  Future<void> updateUser() {
    return users
        .doc(this.userId)
        .update({
          'height': int. parse(height),
          'weight': int. parse(weight),
          'age': int. parse(age),
          'gender': gender,
        })
        .then((value) => print("User Updated"))
        .catchError((error) => print("Failed to update user: $error"));
  }
  

  void initState() {
    super.initState();
    _imageURL = FirebaseAuth.instance.currentUser!.photoURL;
    _name = FirebaseAuth.instance.currentUser!.displayName;
    _email = FirebaseAuth.instance.currentUser!.email;
    if (this.userId.isNotEmpty) {
      users.doc(this.userId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data() as Map<String, dynamic>;
          height = data['height'].toString();
          weight = data['weight'].toString();
          age = data['age'].toString();
          gender = data['gender'];
          if(gender==null || gender==''){
            gender='Male';
          }
          _heightController.text = height.toString();
          _weightController.text = weight.toString();
          _ageController.text = age.toString();
          setState(() {
          });
        }
      });
    }
  }

  final _formkey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor:green,
        title: Text('Edit Profile',)
      ),
      body: Container(
        padding: EdgeInsets.only(left: 40, right: 40, top: 20, bottom: 20),
        child: Container(
          padding: EdgeInsets.all(20),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              border: Border.all(color: lightGray, width: 2)),
          child: Form(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            key: _formkey,
            child: Column(
              children: [
                TextFormField(
                  controller: _heightController,
                  decoration: InputDecoration(labelText: 'Height'),
                  onChanged: (value) {
                    setState(() {
                      height = value;
                    });
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please input your Height';
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 45),
                TextFormField(
                  controller: _weightController,
                  decoration: InputDecoration(labelText: 'Weight'),
                  onChanged: (value) {
                    setState(() {
                      weight = value;
                    });
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please input your Weight';
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 45),
                TextFormField(
                  controller: _ageController,
                  decoration: InputDecoration(labelText: 'Age'),
                  onChanged: (value) {
                    setState(() {
                      age = value;
                    });
                  },
                  validator: (value) {
                    if (value == null ||
                        value.isEmpty ||
                        int.tryParse(value) == null) {
                      return 'Please input your Age';
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 45),
                DropdownButtonFormField(
                  value: gender,
                  items: [
                    DropdownMenuItem(
                      child: Row(
                        children: [Text('Male'), SizedBox(width: 8.0)],
                      ),
                      value: 'Male',
                    ),
                    DropdownMenuItem(
                      child: Row(
                        children: [Text('Female'), SizedBox(width: 8.0)],
                      ),
                      value: 'Female',
                    ),
                  ],
                  onChanged: (String? newValue) {
                    setState(() {
                      gender = newValue!;
                    });
                  },
                ),
                const SizedBox(height: 45),
                ElevatedButton(
                  onPressed: () async {
                      if (_formkey.currentState!.validate()) {
                        await updateUser();
                        Navigator.pop(context);
                      }
                    },
                  child: const Text('Save Data'),
                  style: ElevatedButton.styleFrom(
                      shape: StadiumBorder(),
                      primary: green,
                      padding:
                          EdgeInsets.symmetric(horizontal: 30, vertical: 20),
                      textStyle: TextStyle(
                        fontSize: 18,
                      )),
                )
                
              ],
            ),
          ),
        ),
      ),
    );
  }
}
