import 'package:calories_calculator/add_menu.dart';
import 'package:flutter/material.dart';
import 'package:calories_calculator/constants.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

class ListData extends StatefulWidget {
  ListData({Key? key}) : super(key: key);

  @override
  _ListDataState createState() => _ListDataState();
}

class _ListDataState extends State<ListData> {
  late final Stream<QuerySnapshot> _caloriesStream;
  late final Stream<QuerySnapshot> _userStream;
  late CollectionReference list;
  late CollectionReference users;
  late List<dynamic> foods = [];
  late DateTime date;
  late int calwant = 0;
  late String uid;
  late int sumCal = 0;
  late String docId = "";
  late int weight = 0;
  late String gender = "";
  @override
  void initState() {
    super.initState();
    setState(() {
      _caloriesStream = FirebaseFirestore.instance
          .collection('calories')
          .where('uid', isEqualTo: FirebaseAuth.instance.currentUser!.uid)
          .limit(1)
          .snapshots();
      list = FirebaseFirestore.instance.collection('calories');
      _userStream = FirebaseFirestore.instance
          .collection('users')
          .where('uid', isEqualTo: FirebaseAuth.instance.currentUser!.uid)
          .limit(1)
          .snapshots();
      users = FirebaseFirestore.instance.collection('users');
    });
  }

  Future<void> _showDialog(id) {
    Widget cancelButton = TextButton(
      child: new Text("ยกเลิก",style: TextStyle(color: Colors.red),),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop();
      },
    );
    Widget continueButton = TextButton(
      child: Text(
        "ตกลง",
        style: TextStyle(color: green),
      ),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop();
        setState(() {
          late List<dynamic> temp = foods;
          temp.remove(id);
          updateCalories(docId, temp);
          //delUser(id);
        });
      },
    );
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("ลบรายการ"),
          content: Text("คุณต้องการลบรายการ ?"),
          actions: <Widget>[continueButton, cancelButton],
        );
      },
    );
  }

  Future<void> updateCalories(docId, foods) {
    return list
        .doc(docId)
        .update({'foods': foods})
        .then((value) => print('Calories Updateed'))
        .catchError((error) => print('Failed to update Calories: $error'));
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _userStream,
        builder:
            (BuildContext context, AsyncSnapshot<QuerySnapshot> usersnapshot) {
          if (usersnapshot.hasError) {
            return Text('Something went wrong');
          }

          if (usersnapshot.connectionState == ConnectionState.waiting) {
            return Text('Loading');
          }

          usersnapshot.data!.docs.map((DocumentSnapshot document) {
            Map<String, dynamic> data =
                document.data()! as Map<String, dynamic>;
            weight = data['weight'];
            gender = data['gender'];
            if (gender == '' || gender == "Male") {
              calwant = weight * 31;
            } else {
              calwant = weight * 27;
            }
            //int sum = [1, 2, 3].fold(0, (p, c) => p + c);
            for (int i = 0; i < foods.length; i++) {
              sumCal += int.parse(foods[i]['cal'].toString());
            }
          }).toList();

          return StreamBuilder(
              stream: _caloriesStream,
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasError) {
                  return Text('Something went wrong');
                }

                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Text('Loading');
                }

                snapshot.data!.docs.map((DocumentSnapshot document) {
                  Map<String, dynamic> data =
                      document.data()! as Map<String, dynamic>;
                  date = data['date'].toDate();
                  foods = data['foods'];
                  uid = data['uid'];
                  sumCal = 0;
                  docId = document.id;
                  //int sum = [1, 2, 3].fold(0, (p, c) => p + c);
                  for (int i = 0; i < foods.length; i++) {
                    sumCal += int.parse(foods[i]['cal'].toString());
                  }
                }).toList();

                return Container(
                    padding: EdgeInsets.only(
                        left: 40, right: 40, top: 20, bottom: 20),
                    child: Column(children: [
                      Expanded(
                          child: Container(
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(color: lightGray, width: 2)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Foods List',
                              style: TextStyle(fontSize: 18),
                            ),
                            Expanded(
                              child: ListView.builder(
                                padding: const EdgeInsets.all(8),
                                itemCount: foods == null ? 0 : foods.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return Container(
                                    height: 50,
                                    //color: Colors.white,
                                    child: Center(
                                      child: Container(
                                        decoration: BoxDecoration(
                                          border: Border(
                                            bottom: BorderSide(
                                                width: 1, color: lightGray),
                                          ),
                                        ),
                                        child: ListTile(
                                          title: Row(children: [
                                            ClipOval(
                                              child: Material(
                                                color: green,
                                                child: Container(
                                                  width: 15,
                                                  height: 15,
                                                ),
                                              ),
                                            ),
                                            SizedBox(width: 5),
                                            Text('${foods[index]['name']}'),
                                          ]),
                                          trailing: Wrap(
                                            spacing: 12,
                                            children: [
                                              Container(
                                                  padding:
                                                      EdgeInsets.only(top: 11),
                                                  child: Text(
                                                      '${foods[index]['cal']} kcal')),
                                              IconButton(
                                                icon: Icon(Icons.delete),
                                                onPressed: () async {
                                                  await _showDialog(
                                                      foods[index]); //
                                                },
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                            ListTile(
                              visualDensity: VisualDensity(vertical: -4),
                              title: Text('ที่ร่างกายต้องการ'),
                              trailing: Text('${calwant} kcal'),
                            ),
                            ListTile(
                              visualDensity: VisualDensity(vertical: -4),
                              title: Text('ได้รับ'),
                              trailing: Text('${sumCal} kcal'),
                            ),
                            ListTile(
                              visualDensity: VisualDensity(vertical: -4),
                              title: Text('คงเหลือ'),
                              trailing: Text('${calwant - sumCal} kcal'),
                            ),
                          ],
                        ),
                      )),
                      const SizedBox(height: 30),
                      ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context) => AddMenu(docId: docId)),
                          );
                        },
                        child: const Text('เพิ่มรายการอาหาร'),
                        style: ElevatedButton.styleFrom(
                            shape: StadiumBorder(),
                            primary: green,
                            padding: EdgeInsets.symmetric(
                                horizontal: 30, vertical: 20),
                            textStyle: TextStyle(
                              fontSize: 18,
                            )),
                      ),
                    ]));
              });
        });
  }
}
