import 'package:calories_calculator/constants.dart';
import 'package:calories_calculator/widget/profile_widget.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:calories_calculator/edit_profile.dart';

class Profile extends StatefulWidget {
  Profile({Key? key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  int height = 0;
  int weight = 0;
  int age = 0;
  String gender = '';
  int cal = 0;
  String _imageURL = '';
  String _name = '';
  String _email = '';
  late final Stream<QuerySnapshot> _profileStream;
  String docId='';
  @override
  void initState() {
    super.initState();
    setState(() {
      _name = FirebaseAuth.instance.currentUser!.displayName!;
      _imageURL = FirebaseAuth.instance.currentUser!.photoURL!;
      _email = FirebaseAuth.instance.currentUser!.email!;
      _profileStream = FirebaseFirestore.instance
          .collection('users')
          .where('uid', isEqualTo: FirebaseAuth.instance.currentUser!.uid)
          .snapshots();
    });
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _profileStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text('Error');
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text('Loading');
          }

          snapshot.data!.docs.map((DocumentSnapshot document) {
            Map<String, dynamic> data =
                document.data()! as Map<String, dynamic>;
            height = data['height'];
            weight = data['weight'];
            age = data['age'];
            gender = data['gender'];
            if (gender == '' || gender == "Male") {
              cal = weight * 31;
            } else {
              cal = weight * 27;
            }
            docId=document.id;
          }).toList();
          return Container(
            padding: EdgeInsets.all(20.0),
            child: Builder(
              builder: (context) => Scaffold(
                body: ListView(
                  physics: BouncingScrollPhysics(),
                  children: [
                    ProfileWidget(
                      imagePath: _imageURL,
                      onClicked: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                              builder: (context) => EditProfile(userId: docId)),
                        );
                      },
                    ),
                    const SizedBox(height: 30),
                    buildName(),
                    const SizedBox(height: 35),
                    buildData(),
                    const SizedBox(height: 35),
                    buildCalories(),
                    const SizedBox(height: 45),
                    Column(
                      children: [
                        const SizedBox(height: 10),
                        ElevatedButton(
                          onPressed: () async {
                            await FirebaseAuth.instance.signOut();
                          },
                          child: Container(
                            child: Icon(
                              Icons.logout_sharp,
                              color: Colors.white,
                            ),
                          ),
                          style: ElevatedButton.styleFrom(
                              shape: StadiumBorder(),
                              primary: Colors.red,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 30, vertical: 20),
                              textStyle: TextStyle(
                                fontSize: 18,
                              )),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  Widget buildName() => Column(
        children: [
          Text(
            _name,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
          ),
          const SizedBox(height: 10),
          Text(
            _email,
            style: TextStyle(color: Colors.grey),
          )
        ],
      );

  Widget buildData() => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          buildButton(context, "$height", 'Height (cm)'),
          Container(
            height: 20,
            child: VerticalDivider(),
          ),
          buildButton(context, "$weight", 'Weight (kg)'),
          Container(
            height: 20,
            child: VerticalDivider(),
          ),
          buildButton(context, "$age", 'Age (year)'),
          Container(
            height: 20,
            child: VerticalDivider(),
          ),
          buildButton(context, gender, 'Gender'),
        ],
      );

  Widget buildButton(BuildContext context, String text, String value) =>
      MaterialButton(
        padding: EdgeInsets.symmetric(vertical: 4),
        onPressed: () {},
        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              value,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 2),
            Text(
              text,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
            ),
          ],
        ),
      );

  Widget buildCalories() => Column(
        children: [
          Text(
            "Calories For Dairy",
            style: TextStyle(color: Colors.grey),
          ),
          const SizedBox(height: 10),
          Text(
            "$cal",
            style: TextStyle(color: green,fontSize: 35,fontWeight: FontWeight.bold),
          ),
        ],
      );
}
