import 'package:calories_calculator/food.dart';

class Calories {
  int id;
  String date;
  int calwant;
  String uid;
  List<Food> foods = <Food>[];
  Calories(
      {required this.id,
      required this.date,
      required this.calwant,
      required this.uid,
      required this.foods,
      }
  );

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'date': date,
      'calwant': calwant,
      'uid': uid,
      'foods': foods,
    };
  }

  static List<Calories> toList(List<Map<String, dynamic>> maps) {
    return List.generate(maps.length, (i) {
      return Calories(
          id: maps[i]['id'],
          date: maps[i]['date'],
          calwant: maps[i]['calwant'],
          uid: maps[i]['uid'],
          foods: maps[i]['foods']
      );
    });
  }

  @override
  String toString() {
    return 'Todo {$id: $date $calwant $uid $foods';
  }
}
