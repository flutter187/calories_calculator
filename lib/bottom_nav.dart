import 'package:flutter/material.dart';
import 'package:calories_calculator/constants.dart';
import 'package:calories_calculator/list_data.dart';
import 'package:calories_calculator/profile.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
class BottomNavbar extends StatefulWidget {
  const BottomNavbar({Key? key}) : super(key: key);

  @override
  _BottomNavbarState createState() => _BottomNavbarState();
}


class _BottomNavbarState extends State<BottomNavbar> {
  String? _imageURL ;
  String? _name ;
  int _currentIndex = 0;
  @override
  void initState() {
    super.initState();
    setState(() {
      _imageURL = FirebaseAuth.instance.currentUser!.photoURL;
      _name = FirebaseAuth.instance.currentUser!.displayName;
    });
  }

  List<Widget> pageList = <Widget>[
    ListData(),
    Profile()
  ];

  List<String> pageName = <String>[
    'Calorie Calculator',
    'Data Profile',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(pageName[_currentIndex]),foregroundColor:Colors.black, backgroundColor: Colors.white, centerTitle:true),
      body: pageList[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        backgroundColor: lightGray,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.format_list_bulleted),
            label: 'Foods List',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Profile',
          ),
        ],
        onTap: (index){
          setState(() {
            _currentIndex = index;
          });
        },
        selectedItemColor: Colors.green,
      ),
    );
  }
}
