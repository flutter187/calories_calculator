import 'dart:html';

import 'package:calories_calculator/constants.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AddMenu extends StatefulWidget {
  String docId;
  AddMenu({Key? key, required this.docId}) : super(key: key);

  @override
  _AddMenuState createState() => _AddMenuState(this.docId);
}

class _AddMenuState extends State<AddMenu> {
  late String docId;
  List<dynamic> calories = [];
  late final Stream<QuerySnapshot> _profileStream;
  late final Stream<QuerySnapshot> _listStream;
  late CollectionReference foods;
  late List<dynamic> oldfoods = [];

  late final Stream<QuerySnapshot> _foodsStream;
  @override
  void initState() {
    super.initState();
    setState(() {
      _foodsStream = FirebaseFirestore.instance
          .collection('foods')
          .orderBy('id')
          .snapshots();
      foods = FirebaseFirestore.instance.collection('calories');
      _listStream = FirebaseFirestore.instance
          .collection('calories')
          .where('uid', isEqualTo: FirebaseAuth.instance.currentUser!.uid)
          .limit(1)
          .snapshots();
    });
  }

 Future<void> _showDialog(old,addfoods,docId) {
   var name=addfoods['name'];
    Widget cancelButton = TextButton(
      child: new Text("ยกเลิก",style: TextStyle(color: Colors.red),),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop();
      },
    );
    Widget continueButton = TextButton(
      child: Text(
        "ตกลง",
        style: TextStyle(color: green),
      ),
      onPressed: () {
       
        setState(() {
          updateCalories(old,addfoods,docId);
          //delUser(id);
           Navigator.of(context, rootNavigator: true).pop();
           Navigator.pop(context);
        });
      },
    );
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("เพิ่มรายการอาหาร"),
          content: Text("ต้องการเพิ่ม $name หรือไม่"),
          actions: <Widget>[continueButton, cancelButton],
        );
      },
    );
  }

  
  Future<void> updateCalories(old,addfoods,docId) {
    old.add(addfoods);
    print(old);
    print(docId);
    return foods
        .doc(docId)
        .update({'foods': old})
        .then((value) => print('Calories Updated'))
        .catchError((error) => print('Failed to update Calories: $error'));
  }

  CollectionReference users = FirebaseFirestore.instance.collection('users');
  _AddMenuState(this.docId);

  final _formkey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: green,
          title: Text(
            'Add Menu',
          )),
      body: StreamBuilder(
        stream: _listStream,
        builder:
            (BuildContext context, AsyncSnapshot<QuerySnapshot> usersnapshot) {
          if (usersnapshot.hasError) {
            return Text('Something went wrong');
          }

          if (usersnapshot.connectionState == ConnectionState.waiting) {
            return Text('Loading');
          }

          usersnapshot.data!.docs.map((DocumentSnapshot document) {
            Map<String, dynamic> data =
                document.data()! as Map<String, dynamic>;
                oldfoods=data['foods'];
                print(oldfoods);
          }).toList();

          return StreamBuilder(
            stream: _foodsStream,
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (snapshot.hasError) {
                return Text('Something went wrong');
              }

              if (snapshot.connectionState == ConnectionState.waiting) {
                return Text('Loading');
              }
              calories = [];
              snapshot.data!.docs.map((DocumentSnapshot document) {
                Map<String, dynamic> data =
                    document.data()! as Map<String, dynamic>;
                calories.add(data);

                print(calories.toString());
              }).toList();

              return Container(
                padding:
                    EdgeInsets.only(left: 40, right: 40, top: 20, bottom: 20),
                child: Column(children: [
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(20),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: lightGray, width: 2)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Menu List',
                            style: TextStyle(fontSize: 18),
                          ),
                          Expanded(
                            child: ListView.builder(
                              padding: const EdgeInsets.all(8),
                              itemCount: calories == null ? 0 : calories.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Container(
                                  height: 50,
                                  //color: Colors.white,
                                  child: Center(
                                    child: Container(
                                      decoration: BoxDecoration(
                                        border: Border(
                                          bottom: BorderSide(
                                              width: 1, color: lightGray),
                                        ),
                                      ),
                                      child: ListTile(
                                        title: Row(children: [
                                          ClipOval(
                                            child: Material(
                                              color: green,
                                              child: Container(
                                                width: 15,
                                                height: 15,
                                              ),
                                            ),
                                          ),
                                          SizedBox(width: 5),
                                          Text('${calories[index]['name']}'),
                                        ]),
                                        trailing: Wrap(
                                          spacing: 12,
                                          children: [
                                            Container(
                                                padding:
                                                    EdgeInsets.only(top: 11),
                                                child: Text(
                                                    '${calories[index]['cal']} kcal')),
                                            IconButton(
                                              icon: Icon(Icons.add),
                                              onPressed: () async {
                                                await _showDialog(oldfoods,
                                                    calories[index], docId);
                                                 
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ]),
              );
            },
          );
        },
      ),
    );
  }
}
