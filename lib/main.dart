import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:calories_calculator/bottom_nav.dart';
import 'package:calories_calculator/login.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

   runApp(MaterialApp(
    title: 'Calories Calculator',
    theme: ThemeData(fontFamily: 'Kanit'),
    home: MyApp(),
  ));
}


class UnknowPage extends StatefulWidget {
  UnknowPage({Key? key}) : super(key: key);

  @override
  _UnknowPageState createState() => _UnknowPageState();
}

class _UnknowPageState extends State<UnknowPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.red,
    );
  }
}


class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool addState=true;
  final _navigatorKey = GlobalKey<NavigatorState>();
  late StreamSubscription<User?> _sub;
  @override
  void initState() {
    super.initState();
    _sub = FirebaseAuth.instance.authStateChanges().listen((user) {
      _navigatorKey.currentState!
          .pushReplacementNamed(user != null ? 'home' : 'login');
    });
  }

  @override
  void dispose() {
    super.dispose();
    _sub.cancel();
    super.dispose();
  }


  Future <void> checkFirstLogin() async {
    int temp=0;
     await  FirebaseFirestore.instance
              .collection('users')
              .where('uid', isEqualTo: FirebaseAuth.instance.currentUser!.uid)
              .get()
              .then((value){
                temp = value.size;
              });
            if(temp==0 && addState){
              addUser(FirebaseAuth.instance.currentUser!.uid);
              addCalories(FirebaseAuth.instance.currentUser!.uid);
            }
  }

  CollectionReference users = FirebaseFirestore.instance.collection('users');
  Future<void> addUser(uId) {
    addState=false;
    return users
        .add({
          'uid': uId,
          'age': 0,
          'gender': '',
          'height': 0,
          'weight': 0
        })
        .then((value) =>print('User Added'))
        .catchError((error) => print('Failed to add user: $error'));
  }
  CollectionReference calories = FirebaseFirestore.instance.collection('calories');
  Future<void> addCalories(uId) {
    return calories
        .add({
          'uid': uId,
          'date': DateTime.now(),
          'foods': []
        })
        .then((value) => print('Calories Added'))
        .catchError((error) => print('Failed to add Calories: $error'));
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Home',
      navigatorKey: _navigatorKey,
      initialRoute:
          FirebaseAuth.instance.currentUser == null ? 'login' : 'home',
      onGenerateRoute: (setting) {
        switch (setting.name) {
          case 'home':
            //check first login
            checkFirstLogin();
            return MaterialPageRoute(
                settings: setting, builder: (_) => BottomNavbar());
          case 'login':
            return MaterialPageRoute(
                settings: setting, builder: (_) => LoginPage());
          default:
            return MaterialPageRoute(
                settings: setting, builder: (_) => UnknowPage());
        }
      },
    );
  }
}