class Food {
  int id;
  String name;
  int calories;

  Food({required this.id, required this.name, required this.calories});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'calories': calories,
    };
  }

  static List<Food> toList(List<Map<String, dynamic>> maps) {
    return List.generate(maps.length, (i) {
      return Food(
        id: maps[i]['id'],
        name: maps[i]['name'],
        calories: maps[i]['calories'],
      );
    });
  }

  @override
  String toString() {
    return 'Food {$id: $name $calories}';
  }
}
