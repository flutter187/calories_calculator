import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:calories_calculator/widget/delayed_animation.dart';
import 'package:avatar_glow/avatar_glow.dart';
import 'package:calories_calculator/constants.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage>
    with SingleTickerProviderStateMixin {
  final int delayedAmount = 300;
  late double _scale;
  late AnimationController _controller;
  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: Duration(
        milliseconds: 200,
      ),
      lowerBound: 0.0,
      upperBound: 0.1,
    )..addListener(() {
        setState(() {});
      });
    super.initState();
  }

  Future<UserCredential> signInWithGoogle() async {
    // Create a new provider
    GoogleAuthProvider googleProvider = GoogleAuthProvider();

    googleProvider
        .addScope('https://www.googleapis.com/auth/contacts.readonly');
    googleProvider.setCustomParameters({'login_hint': 'user@example.com'});

    // Once signed in, return the UserCredential
    return await FirebaseAuth.instance.signInWithPopup(googleProvider);

    // Or use signInWithRedirect
    // return await FirebaseAuth.instance.signInWithRedirect(googleProvider);
  }

  @override
  Widget build(BuildContext context) {
    final color = Colors.white;
    _scale = 1 - _controller.value;
    return MaterialApp(
      theme: new ThemeData(
        fontFamily: 'Kanit',
      ),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          backgroundColor: green,
          body: Container(
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.all(10.0),
              child: Container(
                  decoration:
                      BoxDecoration(border: Border.all(color: Colors.white, width: 3),borderRadius: BorderRadius.circular(15),),
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 30.0,
                        ),
                        ClipOval(
                           child: Image(image: AssetImage('image/bank.gif')
                             ,
                             width: 100,
                             height: 100,
                             fit: BoxFit.fill,
                           ),
                         ),
                        SizedBox(
                          height: 30.0,
                        ),
                        DelayedAnimation(
                          child: Text(
                            "Calories",
                            style: TextStyle(
                                fontSize: 35.0,
                                color: color),
                          ),
                          delay: delayedAmount + 1000,
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        DelayedAnimation(
                          child: Text(
                            "Calculator",
                            style: TextStyle(
                                fontSize: 35.0,
                                color: color),
                          ),
                          delay: delayedAmount + 2000,
                        ),
                        SizedBox(
                          height: 30.0,
                        ),
                        DelayedAnimation(
                          child: Text(
                            "Login",
                            style: TextStyle(fontSize: 20.0, color: color),
                          ),
                          delay: delayedAmount + 3000,
                        ),
                        SizedBox(
                          height: 30.0,
                        ),
                        DelayedAnimation(
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                padding: const EdgeInsets.all(18),
                                shadowColor: Colors.white,
                            ),
                            onPressed: () async {
                              await signInWithGoogle();
                            },
                            child: Text('Login with Google',
                              style: new TextStyle(
                                fontSize: 24,
                            )),
                          ),
                          delay: delayedAmount + 3000,
                        ),
                      ],
                    ),
                  )))),
    );
  }
}
